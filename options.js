function saveOptions(e) {
    var url = document.getElementById("url").value;

    // Check if last character of the URL is a slash
    // and add one if necessary
    if (url.substr(-1) != "/") {
        url = url + "/";
        document.getElementById("url").value = url;
    }

    let settings = {
        url: url,
        login: document.getElementById("login").value,
        password: document.getElementById("password").value,
        updateInterval: Number(document.getElementById("updateInterval").value),
        bgColor: document.getElementById("bgColor").value
    }
    let setSettings = browser.storage.local.set({settings});

    var sending = browser.runtime.sendMessage({action: "settingsUpdated"});

    e.preventDefault();
}

function restoreOptions() {
    let getSettings = browser.storage.local.get("settings");
    getSettings.then(result => {
        document.getElementById("url").value = result.settings.url;
        document.getElementById("login").value = result.settings.login;
        document.getElementById("password").value = result.settings.password;
        document.getElementById("updateInterval").value = result.settings.updateInterval;
        document.getElementById("bgColor").value = result.settings.bgColor;
    });
}

function localizeOptions() {
    document.getElementById("urlLabel").textContent = browser.i18n.getMessage("UrlTitle");
    document.getElementById("loginLabel").textContent = browser.i18n.getMessage("LoginTitle");
    document.getElementById("passwordLabel").textContent = browser.i18n.getMessage("PasswordTitle");
    document.getElementById("updateIntervalLabel").textContent = browser.i18n.getMessage("UpdateIntervalTitle");
    document.getElementById("bgColorLabel").textContent = browser.i18n.getMessage("BgColorTitle");
    document.getElementById("saveButton").textContent = browser.i18n.getMessage("SaveButtonTitle");
}

document.querySelector("form").addEventListener("submit", saveOptions);
localizeOptions();
restoreOptions();
